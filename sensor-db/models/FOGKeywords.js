const Sequelize = require("sequelize");

module.exports = sequelize.define("FOGKeywords", {

	id: {
		type: Sequelize.INTEGER(11),
		allowNull: false,
		autoIncrement: true,
		primaryKey: true,
	},

	KeywordAddr: {
		type: Sequelize.STRING(250),
		allowNull: false,
	},

	KeywordValue: {
		type: Sequelize.STRING(250),
		allowNull: false
	},

});