'use strict';
const crypto = require('crypto');
const express = require('express');
const request = require('request');

var app = express();
var http = require('http').Server(app);

//Encryption parameters
const key = crypto.randomBytes(32);
//const iv = crypto.randomBytes(16);

//SQL parameters and credentials
var mysql      = require('mysql');
var connection = mysql.createConnection({
  host     : 'localhost',
  user     : 'genius',
  password : '3u3gnE@@2117',
  database : 'fogsse'
});

/*
//Encryption Function
function encryption(text) {
	let cipher = crypto.createCipheriv('aes-256-cbc', Buffer.from(key), iv);
	let ciphertext = cipher.update(text);
	ciphertext = Buffer.concat([ciphertext, cipher.final()]);
	return {
		iv: iv.toString('hex'), encryptedData: ciphertext.toString('hex')
	};
}

//Decryption Function
function decryption(text) {
	let iv = Buffer.from(text.iv, 'hex');
	let encryptedText = Buffer.from(text.encryptedData, 'hex');
	let decipher = crypto.createDecipheriv('aes-256-cbc', Buffer.from(key), iv);
	let decrypted = decipher.update(encryptedText);
	decrypted = Buffer.concat([decrypted, decipher.final()]);
	return decrypted.toString();
}

*/

function keyword_exists(data, addr, nofile) 
{
	connection.beginTransaction(function(err) 
	{
		if (err) {
			console.log(err);
            return;
		}

		var sql = "UPDATE sse_keywords SET sse_keywords_num_file = ? WHERE sse_keywords_word = ?";
		connection.query({
            sql: 'UPDATE sse_keywords SET sse_keywords_num_file = ? WHERE sse_keywords_word = ?',
            values: [nofile+1, data.Temperature]
        }, function (error, results, fields) {
            if(error){
                connection.rollback(function() {
                	console.log(err);
            		return;
                });
            }
        });

        connection.query({
        	sql: 'INSERT INTO sse_csp_keywords(sse_keywords_address,sse_keywords_value) values(?,?)',
            values: [data.Filename, addr]
        }, function (error, results, fields) {
            if(error){
                connection.rollback(function() {
                	console.log(err);
            		return;
                });
            }
        });

        connection.commit(function(err) {
        	if (err) {
        		connection.rollback(function() {
        			console.log(err);
            		return;
        		});
        	}

        	console.log('Database Updated with Values');
        });
	});
}

function keyword_does_not_exist(data, addr, nofile) 
{
	connection.beginTransaction(function(err) 
	{
		if (err) {
			console.log(err);
            return;
		}
		connection.query({
            sql: 'INSERT INTO sse_keywords(sse_keywords_word,sse_keywords_num_file) values(?,?)',
            values: [data.Temperature, nofile]
        }, function (error, results, fields) {
            if(error){
                connection.rollback(function() {
                	console.log(err);
            		return;
                });
            }
        });

        connection.query({
        	sql: 'INSERT INTO sse_csp_keywords(sse_keywords_address,sse_keywords_value) values(?,?)',
            values: [data.Filename, addr]
        }, function (error, results, fields) {
            if(error){
                connection.rollback(function() {
                	console.log(err);
            		return;
                });
            }
        });

        connection.commit(function(err) {
        	if (err) {
        		connection.rollback(function() {
        			console.log(err);
            		return;
        		});
        	}

        	console.log('Database Updated with Values');
        });
	});
}

//Connect to the SQL Database
connection.connect();
app.use(express.static('public'));

app.get('/', function(req, res){
    res.send('Fog SSE DB');
});

//Listening on Port 300
http.listen(3000, function(){
    console.log('listening on *:3000');
    console.log('FOG SSE Implementation is initiated');
});


//Receive data from IoT, Query and Push Data to the SQL Database
var dataPusher = setInterval(function () 
{
    request.get('http://[fd00::212:4b00:18e6:9cf1]/',function(err,res,body){
        if(err){
            console.log(err);
            return;
        }

        //receive and print out content of received data from the IoT device
        var obj = JSON.parse(body);
        console.log(obj);

        console.log('Checking if the File exists in the SQL Database');
        
        //Check for if the filename already exists in the Database
        //Return number of file from the Database

       var numfile;
		connection.query({
            sql: 'SELECT sse_keywords_num_file FROM sse_keywords WHERE sse_keywords_word = ?',
            values: [obj.Temperature]
        }, function (error, result, fields) {
            if(error){
            	console.log(err);
            	return;
        	}
        	if(!result.length){
        		numfile = 0;
        		console.log("Number of Files: " + numfile);
        	}else {
        		numfile = result[0].sse_keywords_num_file;
        		console.log("Number of Files: " + numfile);
        	}
        });

        var numstring = '' + numfile + '';

        //Generate the csp_keyword_address by using HMAC
        var hashtag = crypto.createHmac('sha256', key).update(numstring).digest('hex');
        let buf = Buffer.from(hashtag);
        let csp_addr = buf.toString('base64');
        console.log(csp_addr);

        if (numfile = 0) 
        {
        	keyword_does_not_exists(obj, numfile, csp_addr);
        } else 
        {
        	keyword_exists(obj, numfile, csp_addr);
        }
    });
}, 5000);