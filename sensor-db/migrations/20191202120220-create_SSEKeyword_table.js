'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable("SSEKeywords", {
      id: {
        type: Sequelize.INTEGER(11),
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
      },

      KeywordHash: {
        type: Sequelize.STRING(250),
        allowNull: false,
        unique: true,
      },

      NoApp: {
        type: Sequelize.INTEGER(11),
        allowNull: false,
      },

      NoSearch: {
        type: Sequelize.INTEGER(11),
        allowNull: false,
        defaultValue: '0',
      },

      createdAT: Sequelize.DATE,
      updatedAT: Sequelize.DATE,
    });
   
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable("SSEKeywords");
  }
};
