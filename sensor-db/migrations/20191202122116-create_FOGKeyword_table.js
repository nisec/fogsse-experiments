'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable("FOGKeywords", {
      id: {
        type: Sequelize.INTEGER(11),
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
      },

      KeywordAddr: {
        type: Sequelize.STRING(250),
        allowNull: false,
      },

      KeywordValue: {
        type: Sequelize.STRING(250),
        allowNull: false
      },

      createdAT: Sequelize.DATE,
      updatedAT: Sequelize.DATE,
    });
   
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable("FOGKeywords");
  }
};