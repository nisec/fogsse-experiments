'use strict';
const crypto = require('crypto');
const express = require('express');
const request = require('request');

const readline = require('readline').createInterface({
    input: process.stdin,
    output: process.stdout
});

require("./database/connection");


//Parameters for connecting and creating database objects
const ssekeyword = require('./models/SSEKeywords');
const fogkeyword = require('./models/FOGKeywords');

const errHandler = (err) => {
	console.error("Error: ", err);
}


//Encryption parameters
const key = '65bcb5096d4f31229b597c9700707abb8c70d33c7d01a89f0c5a9218bd1dd0d6';


// Generate Hash of the keyword for searching
function createHash(keyword) {
    return new Promise(function(resolve, reject) {
        //Generate a Hash of the Keyword
        var keystring = '' + keyword + '';
        var hashMsg = crypto.createHash('sha256').update(keystring).digest('bin');
        //console.log(hashMsg);
        let buf = Buffer.from(hashMsg);
        let hash_Msg_to_base64 =  buf.toString('base64');
        resolve(hash_Msg_to_base64);
    });
}

// Retrieve SSE Table for NoApp and NoSearch
function retrieveNoApp(hashedkey) {
    return new Promise(function(resolve, reject) {
        resolve(ssekeyword.findOne({ where: { KeywordHash: hashedkey }}));
    });
}

// Search Fog Tables for Values
function searchSSEFiles(kwaddress) {
    return new Promise(function(resolve, reject) {
        resolve(fogkeyword.findOne({ where: { KeywordAddr: kwaddress }}));
    });
}

// Update Number of Search Column of SSE Table
function updateNoSearch(keyword, numsearch) {
    return new Promise(function(resolve, reject) {
        resolve(ssekeyword.update({ NoSearch: numsearch }, { where: { KeywordHash: keyword } }));
    });
}

// Update the Fog Table with the New Keyword Address
function updateFogTable(oldAddr, newAddr) {
    return new Promise(function(resolve, reject) {
        resolve(fogkeyword.update( { KeywordAddr: newAddr }, { where: { KeywordAddr: oldAddr } }));
    });
}

// generate the address value for the keyword
function createKWaddr(numsearch, numfile, keyword) {
    return new Promise(function(resolve, reject) {
        var tokenstart = process.hrtime();
        var numstring = '' + numsearch + keyword + '';
        var nofilestring = '' + numfile + '';
        //Generate the csp_keyword_address by using HMAC
        var keywordkey = crypto.createHmac('sha256', key).update(numstring).digest('hex');
        var kwaddr = crypto.createHmac('sha256', keywordkey).update(nofilestring).digest('hex');
        let buf = Buffer.from(kwaddr);
        let csp_addr = buf.toString('base64');
        var tokenend = process.hrtime(tokenstart);
        console.log("Token Generation Execution time (hr): ", tokenend[0], tokenend[1]/1000000);
        console.log("\n");
        resolve(csp_addr);
    });
}

// View all entries corresponding to a particular hashed keyword
const loopForValues = async (numapp, numsearch, keyword) => {
    var oldaddress;
    var loopstart = process.hrtime();
    
    for (var i = 1; i <= numapp; i++) 
    {
        var promise1 = await createKWaddr(numsearch, i, keyword);
        oldaddress = promise1;
        var promise2 = await searchSSEFiles(promise1);
        if (promise2 === null) {
            console.log("KEYWORD ADDRESS NOT EXIST");
            return process.exit(22);
        }

        console.log("\n.............................................................")
        console.log(i + "  Keyword Value = " + promise2.KeywordValue);
        console.log(".............................................................\n");

        var promise3 = await createKWaddr(numsearch+1, i, keyword);
        var promise4 = await updateFogTable(oldaddress, promise3);
    }

    var promise5 = await updateNoSearch(keyword, numsearch+1).then(function(result) {
        console.log("Search Complete");
        var loopend = process.hrtime(loopstart);
        console.log("Search Execution time (hr): ", loopend[0], loopend[1]/1000000);
        console.log("\n");
    });
}

readline.question('What measurement are you looking for? ', (keyword) => 
{
    
    var keywd = keyword;
    var KWhash;

    createHash(keywd).then(function(result) {
        console.log("Hash of the Keyword has been created successfully generated: " + result);
        KWhash = result;
        return retrieveNoApp(result);
    }).then(function(result) {
        if (result === null) {
            console.log("KEYWORD DOES NOT EXIST!!!");
            return process.exit(22);
        }

        console.log("\n........................................................");
        console.log("Retrieved number of appearances: " + result.NoApp + " Number of Searches: " + result.NoSearch);
        console.log("........................................................\n");

        loopForValues(result.NoApp, result.NoSearch, KWhash);
        
    });

    readline.close();
});

    

//Error Message
function onErr(err) {
    console.log(err);
    return 1;
}


          