'use strict';
const crypto = require('crypto');
const express = require('express');
const request = require('request');

require("./database/connection");


//Parameters for connecting and creating database objects
const ssekeyword = require('./models/SSEKeywords');
const fogkeyword = require('./models/FOGKeywords');

const errHandler = (err) => {
	console.error("Error: ", err);
}

var app = express();
var http = require('http').Server(app);

//Encryption parameters
const key = '65bcb5096d4f31229b597c9700707abb8c70d33c7d01a89f0c5a9218bd1dd0d6';
//console.log(key);
//const iv = crypto.randomBytes(16);

app.use(express.static('public'));

app.get('/', function(req, res){
    res.send('Fog SSE DB');
});

//Listening on Port 300
http.listen(3000, function(){
    console.log('listening on *:3000');
    console.log('FOG SSE Implementation is initiated');
});

// Initial check to see if the hashed keyword already exists
function getNoFile(hashedkey) {
	return new Promise(function(resolve, reject) {
		resolve(ssekeyword.findOne({ where: { KeywordHash: hashedkey }}));
	});
}

// insert into SSEKeyword table
function insertSSEFiles(hashedkey, noapps) {
	return new Promise(function(resolve, reject) {
		resolve(ssekeyword.create({ KeywordHash: hashedkey, NoApp: noapps }));
	});
}

// update SSE table if the keyword hash already exists
function updateSSEFiles(hashedkey, noapps) {
	return new Promise(function(resolve, reject) {
		resolve(ssekeyword.update({ NoApp: noapps }, { where: { KeywordHash: hashedkey } }));
	});
}

// insert into FOGKeyword table
function insertFOGFiles(kwaddr, kwvalue) {
	return new Promise(function(resolve, reject) {
		resolve(fogkeyword.create({ KeywordAddr: kwaddr, KeywordValue: kwvalue }));
	});
}

// generate the address value for the keyword
function createCSPaddr(numsearch, numfile, keyword) {
	return new Promise(function(resolve, reject) {
		var numstring = '' + numsearch + keyword + '';
		var nofilestring = '' + numfile + '';
        //Generate the csp_keyword_address by using HMAC
        var keywordkey = crypto.createHmac('sha256', key).update(numstring).digest('hex');
        var kwaddr = crypto.createHmac('sha256', keywordkey).update(nofilestring).digest('hex');
        let buf = Buffer.from(kwaddr);
        let csp_addr = buf.toString('base64');
        resolve(csp_addr);
	});
}


//Receive data from IoT, Query and Push Data to the SQL Database
var dataPusher = setInterval(function () 
{
    request.get('http://[fd00::212:4b00:18e6:9cf1]/',function(err,res,body){
        if(err){
            console.log(err);
            return;
        }

        //receive and print out content of received data from the IoT device
        var obj = JSON.parse(body);

        console.log(obj);

        var numfile;
        var numsearch;
        var hrstart = process.hrtime();

        // Check and insert into the Database based on numfile.
        getNoFile(obj.Temperature).then(function(result) {
        	if(result === null) {
        		numfile = 1;
        		numsearch = 0;
        		return insertSSEFiles(obj.Temperature, numfile).then(function(result) {
        			console.log("New Data inserted into Table 1");
        		});
        	} else {
        		numfile = result.NoApp+1;
        		numsearch = result.NoSearch;
        		return updateSSEFiles(obj.Temperature, numfile).then(function(result) {
        			console.log("Number of Appearances has been updated");
        		});
        	}
        }).then(function(result) {
        	return createCSPaddr(numsearch, numfile, obj.Temperature);
        }).then(function(result) {
        	console.log("FOG Address has been successfully generated");
        	return insertFOGFiles(result, obj.Filename);
        }).then(function(result) {
        	console.log("Table 2 has been updated successfully");
        	var hrend = process.hrtime(hrstart);
			console.log("Execution time (hr): ", hrend[0], hrend[1]/1000000);
			console.log("\n\n\n");
        }).catch(function(ex) {
				console.log(ex);
			});
      
    });
}, 5000);


// Rest of the code goes here
//
