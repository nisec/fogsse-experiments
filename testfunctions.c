#include "contiki.h"
#include "myencrypt.h"
#include "sha256.h"
#include "test_uti.h"
#include "base64.h"

#include <clock.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <inttypes.h>

int SHA_256(char *msg, uint8_t *hashtag)
{

  struct tc_sha256_state_struct sh;
  (void)tc_sha256_init(&sh);
  tc_sha256_update(&sh, (const uint8_t *) msg, strlen(msg));
  (void)tc_sha256_final(hashtag, &sh);

  return 0;       
}

PROCESS(test_functions, "Test Server");
AUTOSTART_PROCESSES(&test_functions);

PROCESS_THREAD(test_functions, ev, data)
{
  	PROCESS_BEGIN();
	//Generate dummy temperature figures
	
  	for (int i = 10; i < 41; i++)
  	{
		int temperature = i;
		char temp[10];
		sprintf(temp, "%d", temperature);
		printf("Temperature: %d \n", temperature);


		  //Hashing the keyword values (keyword hashing)
		uint8_t hash_temperature[32];
		SHA_256(temp, hash_temperature);
		show_str("Hashed Temperature:" ,hash_temperature, sizeof(hash_temperature));

		  //Encoding the hashed keyword to base64 for sending
		char *hash_temperature_base64;
		hash_temperature_base64 = b64_encode(hash_temperature, sizeof(hash_temperature));
		printf("Encoded Hashed Temperature = %s\n", hash_temperature_base64);

		free(hash_temperature_base64);

	}

	PROCESS_END();
}

  
