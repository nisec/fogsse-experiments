# FogSSE Experiments

A forward private dynamic SSE scheme that runs on resource constrained devices with the aid of a Fog node. The implementation uses the Contiki-NG OS for the IoT device (Zolertia Re-Mote board).

# FogSSE Architecture

The proposed FogSSE scheme involves a Fog Node (FN), an IoT sensor node and a Cloud Service Provider (CSP). This implementation of the experiments focuses on the FN and the IoT sensor node. The experiments specifically focuses on:
* Add algorithm on the Sensor side
* Add algorithm on the FN side
* Search token generation

# IoT Sensor Experiments

Functionalities of this phase of the experiments are built on top of the Contiki-NG IoT operating system. To this end, to successfully run the experiments on the sensor device, Contiki-NG has to be downloaded from https://github.com/contiki-ng/contiki-ng. This repository should be copied into the "contiki-ng/examples/" directory for use. Code in this section is written in the C programming language.

## dependencies

* `base64.c` : base64 encoding of uint8_t to char
* `myencrypt.c` : modified tinycrypt implementation of AES128 encryption scheme.
* `sha256.c` : tinycrypt implementation of SHA256 hash function

Tinycrypt library: https://github.com/intel/tinycrypt

## run sensor experiments 

* Specify the IoT sensor board and target platform being used for the experiments. This work uses the Zolertia Re-mote Revb board which is based on "Zoul" target platform:\
    `$ make TARGET=zoul BOARD=remote-revb savetarget`

* Build and make the source code file for the IoT sensor board. Take note of the specific port that the sensor is connected to (e.g. USB1): \
    `$ make add_algorithm_iot_client.upload PORT="/dev/ttyUSB1"`

* Log in to the sensor device to view its operations" \
    `$ make login`

# Fog Node Experiments

This section is written in Nodejs and can be run outside of the contiki-ng environment. The source code is found in the /sensor-db/ folder. Two operations are performed on the FN; the add algorithm on the FN side `add_algorithm_fog_client.js` and the search algorithm `search_algorithm.js`. Database parameters are configured in the `config.json` file in "/sensor-db/config/" and the `connection.js` file in "/sensor-db/database/".

## run FN experiments (add algorithm)

* Specify IPV6 address of the sensor node sensor sending data to the FN (line 83 of `add_algorithm_fog_client.js`): \
    `request.get('http://[fd00::212:4b00:18e6:9cf1]/',function(err,res,body) ... `

* Run the script: \
    `node add_algorithm_fog_client.js`

## run FN experiments (search algorithm)

* Run the search algorithm script: \
    `node search_algorithm.js`

# Credits

## Authors

* Eugene Frimpong (Tampere University, Tampere, Finland)
* Alexandros Bakas (Tampere University, Tampere, Finland)
* Hai-Van Dang (University of Westminster, London, United Kingdom)
* Antonios Michalas (Tampere University, Tampere, Finland)

## Funding

This research has received funding from the ASCLEPIOS: Advanced Secure Cloud Encrypted Platform for Internationally Orchestrated Solutions in Healthcare Project No. 826093 EU research project and the European Union’s Horizon
2020 research and innovation Programme under grant agreement No 825355 (CYBELE)