#ifndef __TC_MYENCRYPT_H__
#define __TC_MYENCRYPT_H__

#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif

#ifndef NULL
#define NULL ((void *)0)
#endif

#define Nb (4)  /* number of columns (32-bit words) comprising the state */
#define Nk (4)  /* number of 32-bit words comprising the key */
#define Nr (10) /* number of rounds */
#define TC_AES_BLOCK_SIZE (Nb*Nk)
#define TC_AES_KEY_SIZE (Nb*Nk)

#define TC_CRYPTO_SUCCESS 1
#define TC_CRYPTO_FAIL 0

#define TC_ZERO_BYTE 0x00

typedef struct tc_aes_key_sched_struct 
{
	unsigned int words[Nb*(Nr+1)];
} *TCAesKeySched_t;

#define default_RNG_defined 1

int default_CSPRNG(uint8_t *dest, unsigned int size);
/**
 *  @brief Set AES-128 encryption key with the input key to be used. */
int tc_aes128_set_encrypt_key(TCAesKeySched_t s, const uint8_t *k);

/**
 *  @brief AES-128 Encryption procedure. Encrypts contents of in buffer into out buffer under the set key; */
int tc_aes_encrypt(uint8_t *out, const uint8_t *in, const TCAesKeySched_t s);

/**
 *  @brief Set the AES-128 decryption key with the key to be used. Similar to that of the encryption key. */
int tc_aes128_set_decrypt_key(TCAesKeySched_t s, const uint8_t *k);

/**
 *  @brief AES-128 Decryption procedure. Decrypts in buffer into out buffer under the set key; */
int tc_aes_decrypt(uint8_t *out, const uint8_t *in, const TCAesKeySched_t s);


#ifdef __cplusplus
}
#endif

#endif /* __TC_MYENCRYPT_H__ */

